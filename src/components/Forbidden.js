import { Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import styles from "./Forbidden.module.css";


export default function Forbidden() {
    let history = useHistory();
    function  navigateBack() {
        history.push("/home"); 
    }
    return (
        <div className={
            styles.container
        }>
            <div className={
                styles.forbidden

            }
            >
                <h1>Forbidden</h1>
                <div>placeholder</div>
                <Button variant="contained" color="primary" onClick={navigateBack}>
                    Vissza a kezdő oldalra
                </Button>
            </div>




        </div>

    )
}