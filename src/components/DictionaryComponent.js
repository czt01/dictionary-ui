import { Button } from "@material-ui/core";
import classes from "./DictionaryComponent.module.css";
import { useHistory } from 'react-router-dom';

export default function DictionaryComponent(props) {
    const history = useHistory();

    function deleteItem(id) {
        props.handleDelete(id);
    }

    function openDict(id) {
        history.push(`/mydict/${id}`);
    }

    return (

        < div key={props.id} className={classes.boxes} >
            <div>
                <p>{props.name}</p>
                <p>{props.date}</p>
                <div className={classes.container}>
                    <Button
                        className={classes.flexbutton}
                        variant="outlined"
                        onClick={() => openDict(props.id)}
                    >
                        Open
                    </Button>
                    <Button
                        className={classes.flexbutton}
                        variant="outlined"
                        color="secondary"
                        onClick={() => deleteItem(props.id)}
                    >
                        Delete
                    </Button>
                </div>
            </div>
        </div >
    )
}