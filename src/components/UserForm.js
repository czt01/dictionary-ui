import { useHistory } from "react-router";
import style from "./UserForm.module.css";
import Paper from '@material-ui/core/Paper';
import { Typography } from "@material-ui/core";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import SaveIcon from '@material-ui/icons/Save';
import { useState } from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import HomeIcon from '@material-ui/icons/Home';

export default function UserForm() {

    const [loading, setLoading] = useState(false);

    const history = useHistory();

    function submitChange() {
        //TODO render changes
        setLoading(true);
        //aweit fetch
        //setLoading(false);

        //history.push("/login");
    }

    function navigateBackToHome() {
        history.push("/home");
    }



    return (
        <div className={style.container}>
            <Paper elevation={3} className={style.user}>
                <Typography variant="h3" component="h4">
                    My Details
                </Typography>

                <TextField className={style.outlinedbasic} label="e-mail" variant="outlined" />
                <Typography variant="h4" component="h4">
                    Password Change
                </Typography>

                <TextField className={style.outlinedbasic} label="Current password" variant="outlined" />
                <TextField className={style.outlinedbasic} label="New password" variant="outlined" />
                <TextField className={style.outlinedbasic} label="Password again" variant="outlined" />
                <div className={style.buttons}>
                    <Button disabled={loading} onClick={submitChange} variant="contained" color="primary" className={style.button1} startIcon={loading ?  <CircularProgress size={24} /> : <SaveIcon />}>
                        {loading?'saving' : 'Save'} 
                    </Button>
                    <Button onClick={navigateBackToHome} startIcon={<HomeIcon />} variant="contained" color="primary" className={style.button2}>
                        Back to home
                    </Button>
                </div>
            </Paper>
        </div>
    )
}