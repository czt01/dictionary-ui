import { useHistory } from "react-router-dom";
import style from './Login.module.css';
import {
    Button,
    TextField,
    FormControl,
    CircularProgress
} from "@material-ui/core";
import { useState } from "react";
import UserService from "../services/user-service";

export default function Login() {
    const history = useHistory();

    const [userData, setUserdata] = useState({
        username: '',
        password: '',
    });

    const [inputSent, setInputSent] = useState({
        handler: true,
    });

    function navigateToRegister() {
        history.push("/register");
    }

    async function handleButton() {
        setInputSent({
            handler: false,
        });

        const resp = await UserService.loginUser(userData);

        if (resp.success) {
            history.push('/mydict');
        }
    }

    function handleChange( event ) {
        // console.log(event.target.value, event.target.id);

        const { value, id } = event.target;
        setUserdata({ ...userData, [id]: value });
        console.log(userData)
    }

    return (
        <div className={style.main}>
            <FormControl>
                <TextField 
                    id="username"
                    value={userData.username}
                    onChange={handleChange}
                    className={style.textField} 
                    label="Username" 
                    disabled={!inputSent.handler}>
                </TextField>
                <TextField 
                    id="password"
                    value={userData.password}
                    onChange={handleChange}
                    className={style.textField} 
                    type="password" 
                    disabled={!inputSent.handler} 
                    label="Password">
                </TextField>
                {
                    inputSent.handler 
                        ? 
                            <Button className={style.button} variant="outlined" size="large" onClick={handleButton}>Login</Button>
                        :
                            <CircularProgress className={style.spinner} />

                }

            </FormControl>
            <p>or <Button color="inherit" variant="outlined" size="small" onClick={navigateToRegister}>Sign Up</Button> now</p>

        </div>

    )
}