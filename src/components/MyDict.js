import { useEffect, useState } from 'react';
import classes from './MyDict.module.css';
import { Button, Input } from '@material-ui/core';
import DictionaryComponent from './DictionaryComponent';
import SheetService from '../services/sheet-service';

export default function MyDict() {
    const [dictionaries, setDictionary] = useState([
        {
            dictName: '',
            date: '',
            id: -1,
        }
    ]);

    const [newDict, setNewDict] = useState(
        {
            newDictName: '',
        }
    );

    useEffect(() => {
        async function loadData() {
            const dictionarys = await SheetService.retriveAllSheet();
            console.log(dictionarys);
            setDictionary(dictionarys);
        }
        loadData();
    }, [])

    function deleteItem(id) {
        const items = [...dictionaries];
        const item = items.find((i) => i.id === id);
        if (item) {
            setDictionary(items.filter((i) =>
                i !== item
            ));
        }
    }

    async function addNewDict() {
        if (newDict.newDictName) {
            const sheetData = {
                name: newDict.newDictName,
                description: ""
            };

            await SheetService.addNewSheet(sheetData);

            setDictionary([...dictionaries, sheetData]);

        }
        setNewDict(
            {
                newDictName: '',
            },
        );
    }

    function generateNewId(id) {
        const item = dictionaries.find((i) => i.id === id);
        if (item) {
            return generateNewId(id + 1);
        }
        return id;
    }

    function handleChange(event) {
        setNewDict(
            {
                newDictName: event.target.value,
            },
        );
    }

    return (
        <div className={classes.container}>

            <div className={classes.addNewElement}>
                <Button
                    className={classes.addbutton}
                    variant="outlined"
                    color="primary"
                    size="large"
                    onClick={addNewDict}
                >
                    ADD NEW
                </Button>
                <Input
                    className={classes.addInputField}
                    type="text"
                    placeholder="new dictionary"
                    onChange={(event) => handleChange(event)}
                />
            </div>
            <br />

            {
                dictionaries.map((item) =>
                    item.id ?
                        <DictionaryComponent key={item.id} {...item} handleDelete={deleteItem} />
                        :
                        ''
                )
            }
        </div>
    )
}