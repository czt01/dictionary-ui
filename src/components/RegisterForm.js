import { TextField, FormControl, Button } from "@material-ui/core";
import Alert from '@material-ui/lab/Alert';
import { useState } from "react";
import classes from './RegisterForm.module.css';
import UserService from '../services/user-service';
import { useHistory } from "react-router-dom";

export default function RegisterForm(props) {

    const history = useHistory();

    const [error, setError] = useState("");
    const [regDataStates, setRegData] = useState([
        // {
        //     value: '',
        //     placeholder: 'Username',
        //     type: 'text',
        //     valid: true,
        //     id: 1,
        //     error: 'username too short',
        // },
        {
            value: '',
            name: 'username', 
            placeholder: 'Email',
            type: 'email',
            valid: true,
            id: 2,
            error: 'email form not correct',
        },
        {
            value: '',
            name: 'password',
            placeholder: 'Password',
            type: 'password',
            valid: true,
            id: 3,
            error: 'short password , not secure enough',
        },
        {
            value: '',
            name: 'passwordAgain',
            placeholder: 'Password Again',
            type: 'password',
            valid: true,
            id: 4,
            error: 'password mismatch',
        }
    ]);

    async function submit() {
        const items = [...regDataStates];
        // items.forEach((item) => {
        //     const length = item.value.length;
        //     const num = item.id;

        //     switch (num) {
        //         case 1:
        //             if (length < 5) {
        //                 item.valid = false;
        //             }
        //             setRegData(items);
        //             break;
        //         case 2:
        //             if (!item.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
        //                 item.valid = false;
        //             }
        //             setRegData(
        //                 items
        //             )
        //             break;
        //         case 3:
        //             if (length < 7) {
        //                 item.valid = false;
        //             }
        //             setRegData(
        //                 items
        //             )
        //             break;
        //         case 4:
        //             if (item.value !== items[2].value) {
        //                 item.valid = false;
        //             }
        //             setRegData(
        //                 items
        //             )
        //             break;
        //         default:
        //             break;
        //     }
        // });

        if (items[0].valid && items[1].valid && items[2].valid) {
            // props.addUserToState(items);

            const userData = {};

            items.forEach( field => {
                userData[field.name] = field.value;
            })
            
            const result = await UserService.registerUser(userData);
            
            if (result.success) {
                history.push('/login');
            }

            if (result.error) {
                setError(result.error);
            }
        }
    }

    function handleChange(event, id) {
        const items = [...regDataStates];
        const item = items.find((i) => i.id === id);
        if (item) {
            item.value = event.target.value;
        }
    } // value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) 123456789

    return (
        <div className={classes.register}>
            <h1>Register Form</h1>
            <FormControl>
                {
                    regDataStates.map((item) =>
                        <>
                            <TextField
                                key={item.id}
                                label={item.placeholder}
                                type={item.type}
                                onChange={(event, id) => handleChange(event, item.id)}
                                value={regDataStates.value}
                            />
                            <div className="validity" >{item.valid ? '' : item.error}</div>
                        </>
                    )
                }
                <br />
 
                {
                    error && <div>
                        <Alert severity="error">{error}</Alert>
                    </div>
                }

                <Button
                    type="submit"
                    onClick={submit}
                    size="large"
                    variant="outlined"
                >
                    Register
                </Button>
                
            </FormControl>
        </div>
    );
}