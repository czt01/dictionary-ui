export default class SheetService {

    static async retriveAllSheet() {

        try {
            const token = window.localStorage.getItem('token');
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/sheets`,
                {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                }
            );
            const result = await response.json();
            const { data } = result;

            return data;

        } catch (e) {
            console.log(e);
            return [];   
        }
    }

    static async addNewSheet(sheetData) {

        try {
            const token = window.localStorage.getItem('token');
            const response = await fetch(
                `${process.env.REACT_APP_SERVER_ADDRESS}/sheets`,
                {
                    method: 'POST',
                    body: JSON.stringify(sheetData),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    },
                }
            );
            const result = await response.json();
            const { data } = result;

            return result;
        } catch (e) {
            console.log(e);
            return {
                "success": false,
                "error": "Unexpected server error!"
            }
        }
    }
}