import './App.css';
import PrimarySearchAppBar from './components/PrimarySearchAppBar';
import Home from './components/Home';
import Login from './components/Login';
import RegisterForm from './components/RegisterForm';
import MyDict from './components/MyDict';
import Forbidden from './components/Forbidden'
import UserForm from './components/UserForm';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

export default function App() {
  
  return (
    <Router>
      <div>
        <PrimarySearchAppBar />

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/forbidden">
            <Forbidden />
            </Route>
          <Route path="/register">
            <RegisterForm />
          </Route>
          <Route path="/mydict">
            <MyDict 
            />
          </Route>
          <Route path = "/user">
            <UserForm/>
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}